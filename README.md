# OpenML dataset: Soybean-price-factor-data-1962-2018

https://www.openml.org/d/43576

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Soy beans are a major agricultural crop.
Content
Compilation of Soybean prices and factors that effect soybean prices. Daily data. Temp columns are daily temperatures of the major U.S. grow areas. Production and Area are the annual counts from each country (2018 being the estimates). Prices of commodities are from CME futures and are NOT adjusted for inflation. Updates of these CME futures can be found on Quandl. Additional data could be added, such as, interest rates, country currency prices, country import data, country temperatures.
More raw data I used to assemble this.  
https://github.com/MotorCityCobra/Soy_Data_Collection
Browse my other projects and offer me a job.
Acknowledgements
https://www.quandl.com/
Banner Photo by rawpixel on Unsplash

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43576) of an [OpenML dataset](https://www.openml.org/d/43576). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43576/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43576/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43576/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

